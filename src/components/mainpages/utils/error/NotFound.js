import React from "react";

function NotFound() {
    return (
        <div>
            <br></br>
            <center> This Page Isn't Available. </center>
            <br></br>
            <center>The link may be broken, or the page may have been removed. Check to see if the link you're trying to open is correct.</center>           
        </div>
    )
}

export default NotFound;