import React from "react";
import BtnRender from './BtnRender';

function ProductItem({product}) { 
    return (
        <div className="productCard">
            <img src={product.images.url} alt="" />

            <div className="productBox">
                <h2 title = {product.title}> {product.title} </h2>
                <span>${product.price}</span>
                <p>{product.description}</p>
            </div>

            
            <BtnRender product={product} />
        </div>
    )
}

export default ProductItem;